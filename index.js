/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

	function firstNumber(number){
		let firstNumber = number + 15;
		console.log("Display sum of " + number + " and 15");
		console.log(firstNumber);
		
	};

	firstNumber(5);

	function secondNumber(number){
		let secondNumber = 20 - number;
		console.log("Display difference of " + number + " and 5");
		console.log(secondNumber);
		
	};
	secondNumber(5);

	function thirdNumber(number1){
		console.log("The product of " + number1 + " and 10:");
		return number1;
		
	};
	let number1 = thirdNumber(50 * 10);
	console.log(number1)

	function fourthNumber(number){
		console.log("The quotient of " + number + " and 10:");
		return number; 
	};
	let number = fourthNumber(50 / 10);
	console.log(number);


	function circleArea(radius){
		let pi = 3.1416;
		area = (pi * radius * radius);
		console.log('The result of getting the area of a circle with ' +  radius + ' radius');
		return area;
	}
	let radius = circleArea(15);
	console.log(radius);

	function averageNumber(num1, num2, num3, num4){
		let givenNumbers = num1 + num2 + num3 + num4;
		let givenTotal = givenNumbers / 4;
		console.log("The average of " + num1 + "," + num2 + "," + num3 +  ' and ' + num4);
		return givenTotal;

	};
	let givenTotal= averageNumber(20, 40, 60, 80);
	console.log(givenTotal);

	let isPassingscore = true;
	let isPassed = true;
	function passingGrade(number){
		let score = 38;
		let over = 50;
		average = (score+over)/ number
		let passed = isPassingscore && isPassed;
		console.log('Is '+ score + '/' + over +'  a passing score?');
		return passed;

	};
	let passed = passingGrade(2);
	console.log(passed);

